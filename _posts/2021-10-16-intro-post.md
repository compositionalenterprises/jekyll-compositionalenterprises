---
layout: post
title: Intro Post
---

Welcome to Compositional Enterprises' technical posts!
These are meant to walk you through our technical developments as they come up.
Likely we are going to be talking about these in [the podcast](https://ourcomposecast.com), but we wanted to have some more details written out here.

So, sit back, and enjoy the walkthroughs!
