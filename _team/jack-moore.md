---
title: "Jack Moore"
date: 2018-11-19T10:47:58+10:00
draft: false
image: "images/team/jack.jpg"
jobtitle: "Application Management"
promoted: true
weight: 2
layout: team
---

Jack graduated from The Ohio State University in 2019 with his Bachelor's degree in Computer Science and Engineering. He has been vital in the Open Source community, helping to coordinate local tech conventions such as PyOhio and Ohio Linuxfest during his time in Columbus. Jack is an avid runner, and enjoys making the world a better place, one line of CSS at a time.
