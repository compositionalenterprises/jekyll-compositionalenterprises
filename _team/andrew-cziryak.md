---
title: "Andrew Cziryak"
date: 2018-11-19T10:47:58+10:00
draft: false
image: "images/team/image2.jpeg"
jobtitle: "Infrastructure Operations"
promoted: true
weight: 1
layout: team
---

Andrew has over a decade of personal and professional Linux Systems Administration experience. He holds several industry certifications and most recently is making a foray into the Project Management space. At Compositional Enterprises, he is the Infrastructure Operations Architect/Engineer. His passions include Ansible, Bitcoin, and long walks in the woods.
