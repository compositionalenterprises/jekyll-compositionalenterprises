---
title: "Runners"
date: 2018-11-18T12:33:46+10:00
featured: true
weight: 3
layout: service
---

Runners are our version of automation.
With runners you have the ability to automate any task.
This includes anything from performing an operation when a file is uploaded to a Nextcloud folder to sending weekly reports from Firefly-III.
There are endless possibilities with our runners packages.
We will meet with you to go over your requirements and we will be in contact through the development cycle.
See below for just a few options we have:

* Perform an operation on files when they are uploaded to a specific Nextcloud folder
* Copy files to a specific Nextcloud folder when put into any task in a Kanboard project
* Schedule a run compositional run when a file is uploaded to a Nextcloud Folder
* Schedule a run compositional run when a commit is pushed to a git repo
* Have a weekly report email to you about disk space consumed
* Have a weekly report email about your finance reports in firefly-iii
* Check for nextcloud appliciation updates every Sunday at 3 in the morning
* Have reminder emails kicked off by start dates/due dates in Kanboard tasks.

## Start your Automation Journey

Contact us about Runners to get started.
