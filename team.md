---
title: Team
layout: teams
permalink: /team/
intro_image_absolute: true
intro_image_hide_on_mobile: false
---

# Meet The Team

Small but powerful, the team at Compositional Enterprises is excited to try their hand at implementing Open Source solutions that really make a difference!
