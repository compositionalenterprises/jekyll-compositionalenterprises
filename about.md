---
title: 'About'
date: 2018-02-22T17:01:34+07:00
layout: page
bodyClass: page-about
---

Compositional Enterprises is a company started by Jack Moore and Andrew Cziryak in the heart of the pandemic of 2020.
The aim of this company is first and foremost to promote the ideas of Free, Libre and Open Source Software.
We understand that Open Source Software currently drives some of the most important and critical infrastructure that we depend on in our daily lives.
However, most of that is hidden beneath proprietary layers of client interfaces that negate the hard work that communities all over the world have spent decades investing in.

At Compositional Enterprises, we're out to bring Open Source Software into the limelight.
Our mission is to showcase the usability and practicality of Open Source Software.
We have decided to do this in several ways, by:
- Making open source software available by providing hosting for various end-user web services.
- Providing consulting and educational services to promote practical implementations that work for everyday use.
- Offering our own contributions via automation and infrastructure architecture, as any code we produce under Compositional Enterprises will be open sourced.
- Engaging in the broader community by contributing to any relevant events and conventions
- Making a concerted effort to contribute upstream to the projects that we use in our hosting offerings, in time, energy, and money where appropriate.

By doing all of the above, we aim to bring the proper recognition to open source software projects, as first-class applications.
And by doing so in a sustainable and ethical manner, we will make sure to present ourselves as goodwill ambassadors of Free, Libre, and Open Source Software in its entirety.
