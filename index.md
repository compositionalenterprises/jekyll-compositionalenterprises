---
title: Compositional Enterprises
layout: home
description: Compositional Enterprises is a company that focuses on sustainable promotion and hosting of Free, Libre and Open Source Software.
intro_image: "images/illustrations/pointing.svg"
intro_image_absolute: true
intro_image_hide_on_mobile: true
---

# Compositional Enterprises

Compositional Enterprises is a company that focuses on sustainable promotion and hosting of Free, Libre and Open Source Software with a focus on productivity and collaboration.
